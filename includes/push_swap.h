/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 14:26:12 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:39:23 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

/*
** malloc, free, exit
*/
# include <stdlib.h>

/*
** write (2)
*/
# include <unistd.h>

/*
** read
*/
# include <sys/types.h>
# include <sys/uio.h>

# include "../libft/includes/libft.h"
# include "mlx.h"

# include <stdio.h> // A RETIRER

# define SCREEN_W 1200
# define SCREEN_H 800
# define SPEED 40 // 100 = 1 sec

typedef struct		s_mlx
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	int				done;
	int				speed;
}					t_mlx;

typedef struct		s_rect
{
	int				x;
	int				y;
	int				len;
	int				hei;
}					t_rect;

typedef struct		s_pile
{
	int				nb;
	struct s_pile	*next;
}					t_pile;

typedef struct		s_data
{
	t_pile			**a;
	t_pile			**b;
	int				min;
	int				max;
	int				len;
	t_pile			**size_part;
	int				opt_p;
	int				opt_v;
	t_mlx			*mlx;
}					t_data;

typedef	void		(*t_fins)(t_pile**, t_pile**, char prt);

// PILES
t_pile				*ft_pile_new(int nb);
void				ft_pile_add(t_pile **lst, t_pile *new);
void				ft_pile_append(t_pile **lst, t_pile *new);
void				ft_pile_delone(t_pile **lst);
int					ft_pile_len(t_pile **lst);

// DATA
void				ft_set_data(t_data *new);

// PUSH
void				ft_pa(t_pile **a, t_pile **b, char prt);
void				ft_pb(t_pile **a, t_pile **b, char prt);
void				ft_pa_mult(t_pile **a, t_pile **b, char prt, int mult);

//SWAP
void				ft_sa(t_pile **a, t_pile **b, char prt);
void				ft_sb(t_pile **a, t_pile **b, char prt);
void				ft_ss(t_pile **a, t_pile **b, char prt);
void				ft_opt_sa(t_pile **a, t_pile **b, char prt);

//ROTATE
void				ft_ra(t_pile **a, t_pile **b, char prt);
void				ft_rb(t_pile **a, t_pile **b, char prt);
void				ft_rr(t_pile **a, t_pile **b, char prt);

//REV ROTATE
void				ft_rra(t_pile **a, t_pile **b, char prt);
void				ft_rrb(t_pile **a, t_pile **b, char prt);
void				ft_rrr(t_pile **a, t_pile **b, char prt);

// INST
t_fins				ft_choose_inst(char *inst);
void				read_do_inst(t_data *data, char *str);
int					read_inst_mlx(void *stuff);

//CHECKER
int					ft_checker(t_pile **a, t_pile **b);
int					ft_sorted_inc(t_pile **a);
int					ft_sorted_dec(t_pile **a);
int					ft_sorted_dec_size(t_pile **a, int size);
void				end_check(t_data *data);

//PARSING
int					ft_isdup(t_pile **a, int nb);
t_data				*ft_validate_arg(char **av, int ac);

// DEBUG
void				ft_pile_display(t_pile *lst_a, t_pile *lst_b);

//SELECTION SORT
void				ft_ascend(t_pile **a, int nb);
void				ft_descend(t_pile **a, int nb);
void				ft_pull(t_data *dat);
void				ft_selsort(t_data *dat);

//OTHER
int					mediane(t_pile *pile, int size);

//SORT ?
void				partition_a(t_data *dat, int size);
void				back_up_a(t_data *dat, int size);
void				partition_b(t_data *dat, int size);
void				back_up_b(t_data *dat, int size);
void				ft_quicksort(t_data *dat, int size);

void				ft_quicksort_a(t_data *dat, int size);
void				ft_quicksort_b(t_data *dat);

// VISUALISER
void				ft_img_pixel_put(t_mlx *mlx, int x, int y, int col);
int					ft_init_mlx(t_mlx *mlx);
int					ft_close(t_mlx *mlx);
int					reset_img(t_mlx *mlx);

void				trace_rect(t_mlx *mlx, t_rect rect, int color);
void				visualize(t_mlx *mlx, t_data *data);
int					main_mlx(t_data *data);

#endif
