# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: callard <callard@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/11 11:14:52 by callard           #+#    #+#              #
#    Updated: 2019/02/19 14:37:16 by callard          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#--------VARIABLES-----#
NAME_PS= push_swap
NAME_C= checker

INC_PATH= includes/
SRC_PATH= src/
OBJ_PATH= obj/

SRC_NAME_PS= main_push_swap.c ft_checker.c ft_parsing.c ft_pile.c ft_selsort.c \
		  ft_data.c push.c swap.c rotate.c rev_rotate.c \
		  mediane.c help_debug.c ft_quicksort.c partition.c

SRC_NAME_C= main_checker.c ft_choose_inst.c ft_checker.c ft_parsing.c ft_pile.c \
		  ft_data.c push.c swap.c rotate.c rev_rotate.c \
		  handle_mlx.c visualize.c mediane.c main_mlx.c help_debug.c

OBJ_NAME_PS= $(SRC_NAME_PS:.c=.o)
OBJ_NAME_C= $(SRC_NAME_C:.c=.o)
HEAD_NAME= $(NAME_PS).h

SRC_PS= $(addprefix $(SRC_PATH), $(SRC_NAME_PS))
SRC_C= $(addprefix $(SRC_PATH), $(SRC_NAME_C))
OBJ_PS= $(addprefix $(OBJ_PATH), $(OBJ_NAME_PS))
OBJ_C= $(addprefix $(OBJ_PATH), $(OBJ_NAME_C))
HEAD= $(addprefix $(INC_PATH), $(HEAD_NAME))

LDFLAGS = -Llibft -L/usr/local/lib/
LDLIBS = -lft -lmlx

FRAME= -framework OpenGL -framework AppKit

CFLAGS= -Wall -Wextra -Werror -g
CPPFLAGS = -I$(INC_PATH)
CC= gcc
RM= /bin/rm -rf

#--------RULES---------#
all: $(NAME_PS) $(NAME_C)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(HEAD)
	mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

$(NAME_PS): $(OBJ_PS)
	@make -C libft
	@make -C minilibx
	$(CC) $(FRAME) $(LDFLAGS) $(LDLIBS) $(OBJ_PS) -o $@

$(NAME_C): $(OBJ_C)
	@make -C libft
	@make -C minilibx
	$(CC) $(FRAME) $(LDFLAGS) $(LDLIBS) $(OBJ_C) -o $@

clean:
	$(RM) $(OBJ)
	$(RM) $(OBJ_PATH)
	@make -C libft clean
	@make -C minilibx clean

fclean: clean
	$(RM) $(NAME_PS) $(NAME_C)
	@make -C libft fclean

re: fclean all

norm:
	norminette $(SRC_PATH)
	norminette $(INC_PATH)

.PHONY: all clean fclean re norm
