/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_data.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/27 12:02:08 by callard           #+#    #+#             */
/*   Updated: 2019/08/27 12:02:11 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		ft_set_data(t_data *new)
{
	t_pile		*tmp;

	new->len = ft_pile_len(new->a);
	tmp = *(new->a);
	new->min = tmp->nb;
	new->max = tmp->nb;
	while (tmp)
	{
		if (tmp->nb < new->min)
			new->min = tmp->nb;
		if (tmp->nb > new->max)
			new->max = tmp->nb;
		tmp = tmp->next;
	}
	if (!(new->size_part = (t_pile **)ft_memalloc(sizeof(t_pile*))))
		return ;
	*(new->size_part) = NULL;
	new->mlx = NULL;
}
