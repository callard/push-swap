/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_push_swap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 14:37:43 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 17:13:42 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			main(int ac, char **av)
{
	t_data	*data;

	if (ac == 1)
		return (0);
	if (!(data = ft_validate_arg(&av[1], ac - 2)))
	{
		ft_putendl("Error");
		return (0);
	}
	// ft_selsort(data);
	ft_quicksort_a(data, data->len);
	return (1);
}
