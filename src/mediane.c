/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mediane.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 14:46:57 by callard           #+#    #+#             */
/*   Updated: 2019/09/09 14:49:30 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				getmin(t_pile *pile, int size)
{
	int			min;
	t_pile		*tmp;

	tmp = pile;
	min = tmp->nb;
	while (size > 0 && tmp)
	{
		if (tmp->nb < min)
			min = tmp->nb;
		tmp = tmp->next;
		size--;
	}
	return (min);
}

int				getmax(t_pile *pile, int size)
{
	int			max;
	t_pile		*tmp;

	tmp = pile;
	max = tmp->nb;
	while (size > 0 && tmp)
	{
		if (tmp->nb > max)
			max = tmp->nb;
		tmp = tmp->next;
		size--;
	}
	return (max);
}

static int		findminabove(t_pile *pile, int size, int above)
{
	int			i;
	t_pile		*tmp;
	int			min;

	tmp = pile;
	i = 0;
	min = getmax(pile, size);
	while (i < size && tmp != NULL)
	{
		if (tmp->nb > above && tmp->nb < min)
			min = tmp->nb;
		i++;
		tmp = tmp->next;
	}
	return (min);
}

/*
** If size is odd number : middle number
** If size is even number : X / mediane / X+1
*/

int				mediane(t_pile *pile, int size)
{
	int			i;
	int			nb1;
	int			nb2;
	int			len;

	i = 0;
	nb1 = getmin(pile, size);
	len = (size - 1) / 2;
	while (i < len)
	{
		nb2 = findminabove(pile, size, nb1);
		nb1 = nb2;
		i++;
	}
	return (nb2);
}
