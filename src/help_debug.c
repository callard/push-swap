#include "push_swap.h"

void		ft_pile_display(t_pile *lst_a, t_pile *lst_b)
{
	while (lst_a || lst_b)
	{
		printf("\n");
		if (lst_a)
		{
			printf("%3d", lst_a->nb);
			lst_a = lst_a->next;
		}
		else
			printf("   ");
		if (lst_b)
		{
			printf(" %3d", lst_b->nb);
			lst_b = lst_b->next;
		}
	}
	printf("\n________\n");
	printf("  a   b \n");
}
