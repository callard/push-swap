/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   partition.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 15:38:06 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 16:55:35 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		back_up_a(t_data *dat, int size)
{
	int		i;

	i = 0;
	while (i < (size / 2))
	{
		ft_rra(dat->a, NULL, 'y');
		i++;
	}
}

/*
** If size is odd number : on a X | on b X+1
** If size is even number : on a X | on b X
*/

void		partition_a(t_data *dat, int size)
{
	int		med;
	int		i;
	int		backup;

	med = mediane(*(dat->a), size);
	i = 0;
	backup = ft_pile_len(dat->a) == size ? 0 : 1;
	while (i < size && !(ft_checker(dat->a, dat->b)))
	{
        if ((*(dat->a))->nb <= med)
		{
			ft_pb(dat->a, dat->b, 'y');
		}
        else if (ft_sorted_inc(dat->a) == 1)
            break;
		else
		{
			ft_ra(dat->a, NULL, 'y');
		}
		i++;
	}
	if (backup == 1)
		back_up_a(dat, size);
	ft_pile_add(dat->size_part, ft_pile_new((size + 1) / 2));
}

void		back_up_b(t_data *dat, int size)
{
	int		i;

	i = 0;
	while (i < size)
	{
		ft_rrb(NULL, dat->b, 'y');
		i++;
	}
}

/*
** If size is odd number : on a X | on b X+1
** If size is even number : on a X | on b X
*/

void		partition_b(t_data *dat, int size)
{
	int		med;
	int		i;
	int		backup;
	int		cnt;

	med = mediane(*(dat->b), size);
	i = 0;
	cnt = 0;
	backup = ft_pile_len(dat->b) == size ? 0 : 1;
	while (i < size)
	{
		if ((*(dat->b))->nb <= med)
		{
			ft_rb(NULL, dat->b, 'y');
			cnt++;
		}
		else
			ft_pa(dat->a, dat->b, 'y');
		i++;
	}
	if (backup == 1)
		back_up_b(dat, cnt);
	if (dat->size_part != NULL)
		(*(dat->size_part))->nb -= (size / 2);
}
