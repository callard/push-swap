/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_choose_inst.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 19:04:30 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:31:22 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_fins		ft_choose_inst(char *inst)
{
	if (ft_strcmp(inst, "sa") == 0)
		return (&ft_sa);
	if (ft_strcmp(inst, "sb") == 0)
		return (&ft_sb);
	if (ft_strcmp(inst, "ss") == 0)
		return (&ft_ss);
	if (ft_strcmp(inst, "pa") == 0)
		return (&ft_pa);
	if (ft_strcmp(inst, "pb") == 0)
		return (&ft_pb);
	if (ft_strcmp(inst, "ra") == 0)
		return (&ft_ra);
	if (ft_strcmp(inst, "rb") == 0)
		return (&ft_rb);
	if (ft_strcmp(inst, "rr") == 0)
		return (&ft_rr);
	if (ft_strcmp(inst, "rra") == 0)
		return (&ft_rra);
	if (ft_strcmp(inst, "rrb") == 0)
		return (&ft_rrb);
	if (ft_strcmp(inst, "rrr") == 0)
		return (&ft_rrr);
	return (NULL);
}

void		read_do_inst(t_data *data, char *str)
{
	t_fins	fct;

	if (!(fct = ft_choose_inst(str)))
	{
		ft_putendl("Error");
	}
	else
	{
		fct(data->a, data->b, 'n');
		if (data->opt_p == 1)
			ft_pile_display(*(data->a), *(data->b));
	}
}

int			read_inst_mlx(void *stuff)
{
	t_data		*data;
	char		*str;

	data = (t_data*)stuff;
	if ((get_next_line(0, &str)))
	{
		read_do_inst(data, str);
		visualize(data->mlx, data);
		free(str);
		usleep(data->mlx->speed * 10000);
	}
	else
	{
		if (data->mlx->done == 0)
		{
			end_check(data);
			data->mlx->done = 1;
		}
	}
	return (0);
}
