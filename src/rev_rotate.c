/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_rotate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 12:30:33 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:33:11 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

/*
** rra : rev rotate a - décale d’une position vers le bas tous les élements
**	de la pile a. Le dernier élément devient le premier.
** rrb : rev rotate b - décale d’une position vers le bas tous les élements
**	de la pile b. Le dernier élément devient le premier.
** rrr : rra et rrb en même temps.
*/

void	ft_rra(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;
	t_pile	*tmp_prev;

	if (a == NULL || (*a) == NULL || (*a)->next == NULL)
		return ;
	tmp = (*a)->next;
	tmp_prev = *a;
	while (tmp->next)
	{
		tmp_prev = tmp;
		tmp = tmp->next;
	}
	tmp_prev->next = NULL;
	ft_pile_add(a, tmp);
	b = NULL;
	if (prt == 'y')
		ft_putendl("rra");
}

void	ft_rrb(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;
	t_pile	*tmp_prev;

	if (b == NULL || (*b) == NULL || (*b)->next == NULL)
		return ;
	tmp = (*b)->next;
	tmp_prev = *b;
	while (tmp->next)
	{
		tmp_prev = tmp;
		tmp = tmp->next;
	}
	tmp_prev->next = NULL;
	ft_pile_add(b, tmp);
	a = NULL;
	if (prt == 'y')
		ft_putendl("rrb");
}

void	ft_rrr(t_pile **a, t_pile **b, char prt)
{
	ft_rra(a, b, 'n');
	ft_rrb(a, b, 'n');
	if (prt == 'y')
		ft_putendl("rrr");
}
