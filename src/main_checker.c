/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_checker.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 14:34:38 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:37:23 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			main(int ac, char **av)
{
	char	*str;
	t_data	*data;

	if (ac == 1)
		return (0);
	if (!(data = ft_validate_arg(&av[1], ac - 2)))
	{
		ft_putendl("Error");
		return (0);
	}
	if (data->opt_p == 1)
		ft_pile_display(*(data->a), *(data->b));
	if (data->opt_v == 1 && !main_mlx(data))
		return (0);
	else
	{
		while ((get_next_line(0, &str)))
		{
			read_do_inst(data, str);
			free(str);
		}
		end_check(data);
	}
	return (1);
}
