/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pile.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 15:15:22 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:31:46 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

t_pile		*ft_pile_new(int nb)
{
	t_pile		*new;

	if (!(new = (t_pile *)ft_memalloc(sizeof(t_pile))))
		return (NULL);
	new->next = NULL;
	new->nb = nb;
	return (new);
}

void		ft_pile_add(t_pile **lst, t_pile *new)
{
	new->next = *lst;
	*lst = new;
}

void		ft_pile_append(t_pile **lst, t_pile *new)
{
	t_pile		*tmp;

	if (*lst == NULL)
	{
		*lst = new;
		return ;
	}
	tmp = *lst;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
}

void		ft_pile_delone(t_pile **lst)
{
	t_pile		*old;

	old = *lst;
	if (old != NULL)
	{
		*lst = old->next;
		free(old);
	}
}

int			ft_pile_len(t_pile **lst)
{
	int			len;
	t_pile		*tmp;

	len = 0;
	tmp = *lst;
	while (tmp)
	{
		len++;
		tmp = tmp->next;
	}
	return (len);
}
