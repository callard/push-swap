/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_mlx.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 15:26:55 by callard           #+#    #+#             */
/*   Updated: 2019/11/14 16:00:48 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_deal_key(int key, void *stuff)
{
	t_mlx		*mlx;

	mlx = (t_mlx*)stuff;
	if (key == 53)
		ft_close(mlx);
	else if (key == 69)
	{
		mlx->speed = mlx->speed <= 5 ? 5 : mlx->speed - 5;
	}
	else if (key == 78)
	{
		mlx->speed = mlx->speed >= 100 ? 100 : mlx->speed + 5;
	}
	return (0);
}

int			main_mlx(t_data *data)
{
	ft_set_data(data);
	if (!(data->mlx = (t_mlx*)ft_memalloc(sizeof(t_mlx))))
		return (0);
	if (!(ft_init_mlx(data->mlx)))
		return (0);
	visualize(data->mlx, data);
	mlx_key_hook(data->mlx->win_ptr, ft_deal_key, (void*)data->mlx);
	mlx_loop_hook(data->mlx->mlx_ptr, read_inst_mlx, (void*)data);
	mlx_hook(data->mlx->win_ptr, 17, 0L, ft_close, (void*)data->mlx);
	mlx_loop(data->mlx->mlx_ptr);
	return (1);
}
