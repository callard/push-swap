/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checker.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 14:34:58 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:35:16 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			ft_checker(t_pile **a, t_pile **b)
{
	if (b != NULL && *b != NULL)
		return (0);
	if (a == NULL || *a == NULL)
		return (0);
	return (ft_sorted_inc(a));
}

int			ft_sorted_inc(t_pile **a)
{
	t_pile	*tmp;

	if (a == NULL || *a == NULL)
		return (1);
	tmp = *a;
	while (tmp->next)
	{
		if (tmp->nb > tmp->next->nb)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

int			ft_sorted_dec(t_pile **a)
{
	t_pile	*tmp;

	if (a == NULL || *a == NULL)
		return (1);
	tmp = *a;
	while (tmp->next)
	{
		if (tmp->nb < tmp->next->nb)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

int			ft_sorted_dec_size(t_pile **a, int size)
{
	t_pile	*tmp;

	if (a == NULL || *a == NULL)
		return (1);
	if (size == 0 || size == 1)
		return (1);
	tmp = *a;
	while (tmp->next && size > 1)
	{
		if (tmp->nb < tmp->next->nb)
		{
			return (0);
		}
		tmp = tmp->next;
		size--;
	}
	return (1);
}

void		end_check(t_data *data)
{
	if (ft_checker(data->a, data->b))
		ft_putendl("OK");
	else
		ft_putendl("KO");
}
