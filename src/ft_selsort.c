/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_selsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 17:20:07 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 17:11:39 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ft_ascend(t_pile **a, int nb)
{
	while ((*a)->nb != nb && (*a))
	{
		ft_ra(a, NULL, 'y');
	}
}

void		ft_descend(t_pile **a, int nb)
{
	while ((*a)->nb != nb && (*a))
	{
		ft_rra(a, NULL, 'y');
	}
}

void		ft_pull(t_data *dat)
{
	t_pile		*tmp;
	int			min;
	int			i;
	int			pos;

	tmp = *(dat->a);
	min = tmp->nb;
	i = 0;
	while (tmp)
	{
		if (tmp->nb < min)
		{
			min = tmp->nb;
			pos = i;
		}
		i++;
		tmp = tmp->next;
	}
	if (pos < (ft_pile_len(dat->a)) / 2)
		ft_ascend(dat->a, min);
	else
		ft_descend(dat->a, min);
}

void		ft_selsort(t_data *dat)
{
	int		cnt;

	cnt = 0;
	ft_opt_sa(dat->a, dat->b, 'y');
	while (ft_sorted_inc(dat->a) == 0 || ft_sorted_dec_size(dat->b, cnt) == 0)
	{
		ft_pull(dat);
		if (ft_sorted_inc(dat->a) == 0 || ft_sorted_dec_size(dat->b, cnt) == 0)
		{
			ft_pb(dat->a, dat->b, 'y');
			cnt++;
		}
	}
	while (cnt > 0 && *(dat->b))
	{
		ft_pa(dat->a, dat->b, 'y');
		cnt--;
	}
}
