/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_quicksort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 16:00:10 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 17:13:09 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ft_quicksort_a(t_data *dat, int size)
{
	if (ft_sorted_inc(dat->a) == 1 && *(dat->b) == NULL)
		return ;
	else if (ft_sorted_inc(dat->a) == 1 && *(dat->b) != NULL)
	{
		ft_quicksort_b(dat);
	}
	else if (size <= 3)
	{
		ft_selsort(dat);
		ft_quicksort_b(dat);
	}
	else
	{
		partition_a(dat, size);
		ft_quicksort_a(dat, size / 2);
	}
}

void		ft_quicksort_b(t_data *dat)
{
	int	nb;

	nb = 0;
	if (*(dat->b) == NULL || *(dat->size_part) == NULL)
		return ;
	else if (ft_sorted_dec(dat->b) == 1)
	{
		ft_pa_mult(dat->a, dat->b, 'y', ft_pile_len(dat->b));
		return ;
	}
	else if ((*(dat->size_part))->nb <= 2)
	{
		nb = (*(dat->size_part))->nb;
		ft_pa_mult(dat->a, dat->b, 'y', nb);
		ft_pile_delone(dat->size_part);
		ft_quicksort_a(dat, nb);
	}
	else
	{
		partition_b(dat, (*(dat->size_part))->nb);
		ft_quicksort_a(dat, (*(dat->size_part))->nb / 2);
	}
}
