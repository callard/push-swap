/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_mlx.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 15:52:28 by callard           #+#    #+#             */
/*   Updated: 2019/10/08 15:52:31 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ft_img_pixel_put(t_mlx *mlx, int x, int y, int col)
{
	char		*img;
	int			bpp;
	int			lsize;
	int			endian;

	img = mlx_get_data_addr(mlx->img_ptr, &bpp, &lsize, &endian);
	if (x < SCREEN_W && y < SCREEN_H)
	{
		if ((4 * x + y * lsize) < 4 * SCREEN_W * SCREEN_H)
		{
			img[4 * x + y * lsize] = (unsigned int)(col & 0x0000FF);
			img[(4 * x + y * lsize) + 1] =
				(unsigned int)((col & 0x00FF00) >> 8);
			img[(4 * x + y * lsize) + 2] = (unsigned int)((col >> 16));
		}
	}
}

static void		mid_line(t_mlx *mlx)
{
	t_rect		mid_line;

	mid_line.x = SCREEN_W / 2;
	mid_line.y = 0;
	mid_line.len = 1;
	mid_line.hei = SCREEN_H;
	trace_rect(mlx, mid_line, 0xFFFFFF);
}

int				reset_img(t_mlx *mlx)
{
	mlx_clear_window(mlx->mlx_ptr, mlx->win_ptr);
	if (mlx->img_ptr != NULL)
		mlx_destroy_image(mlx->mlx_ptr, mlx->img_ptr);
	if (!(mlx->img_ptr = mlx_new_image(mlx->mlx_ptr, SCREEN_W, SCREEN_H)))
		return (0);
	mid_line(mlx);
	return (1);
}

int				ft_init_mlx(t_mlx *mlx)
{
	if (!(mlx->mlx_ptr = mlx_init()))
		return (0);
	if (!(mlx->win_ptr = mlx_new_window(mlx->mlx_ptr, SCREEN_W, SCREEN_H,
		"push_swap")))
		return (0);
	mlx->img_ptr = NULL;
	if (!reset_img(mlx))
		return (0);
	mlx->done = 0;
	mlx->speed = SPEED;
	return (1);
}

int				ft_close(t_mlx *mlx)
{
	mlx_destroy_window(mlx->mlx_ptr, mlx->win_ptr);
	free(mlx);
	exit(EXIT_SUCCESS);
}
