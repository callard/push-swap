/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 12:11:41 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:33:23 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

/*
** ra : rotate a - décale d’une position vers le haut tous les élements
**	de la pile a. Le premier élément devient le dernier.
** rb : rotate b - décale d’une position vers le haut tous les élements
**	de la pile b. Le premier élément devient le dernier.
** rr : ra et rb en même temps.
*/

void	ft_ra(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;

	if (a == NULL || (*a) == NULL || (*a)->next == NULL)
		return ;
	tmp = *a;
	*a = (*a)->next;
	tmp->next = NULL;
	ft_pile_append(a, tmp);
	b = NULL;
	if (prt == 'y')
		ft_putendl("ra");
}

void	ft_rb(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;

	if (b == NULL || (*b) == NULL || (*b)->next == NULL)
		return ;
	tmp = *b;
	*b = (*b)->next;
	tmp->next = NULL;
	ft_pile_append(b, tmp);
	a = NULL;
	if (prt == 'y')
		ft_putendl("rb");
}

void	ft_rr(t_pile **a, t_pile **b, char prt)
{
	ft_ra(a, b, 'n');
	ft_rb(a, b, 'n');
	if (prt == 'y')
		ft_putendl("rr");
}
