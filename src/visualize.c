/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 15:03:53 by callard           #+#    #+#             */
/*   Updated: 2019/09/17 16:41:30 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			trace_rect(t_mlx *mlx, t_rect rect, int color)
{
	int			x;
	int			y;

	x = rect.x;
	while (x < (rect.x + rect.len))
	{
		y = rect.y;
		while (y < (rect.y + rect.hei))
		{
			ft_img_pixel_put(mlx, x, y, color);
			y++;
		}
		x++;
	}
}

static int		choose_color(t_data *data, int nb)
{
	int			color;
	int			gradient;
	int			x;

	gradient = data->len;
	color = 0xFFFFFF;
	x = 1;
	if (nb == 0)
		color = 0xFFFFFF;
	else if (nb < 0)
	{
		gradient = -data->min;
		x = ((255 * nb) / gradient);
		color = ft_rgb(255, x, 255);
	}
	else
	{
		gradient = data->max;
		x = 255 - ((255 * nb) / gradient);
		color = ft_rgb(x, x, 255);
	}
	return (color);
}

static void		see_pile(t_data *data, t_rect rect, t_pile *tmp, int aoub)
{
	int			max;

	max = data->max > -data->min ? data->max : -data->min;
	while (tmp && rect.y < SCREEN_H)
	{
		if (tmp->nb == 0)
			rect.len = 1;
		else
			rect.len = (int)(ft_abs(tmp->nb) * ((int)(SCREEN_W / 2) - 2) / max);
		rect.x = (aoub * (int)(SCREEN_W / 4)) - rect.len / 2;
		trace_rect(data->mlx, rect, choose_color(data, tmp->nb));
		rect.y += rect.hei + 1;
		tmp = tmp->next;
	}
}

void			visualize(t_mlx *mlx, t_data *data)
{
	t_rect		rect;
	t_pile		*tmp;

	reset_img(mlx);
	rect.hei = SCREEN_H / data->len - 1;
	rect.x = 1;
	rect.y = 1;
	tmp = *(data->a);
	see_pile(data, rect, tmp, 1);
	rect.y = 1;
	tmp = *(data->b);
	see_pile(data, rect, tmp, 3);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win_ptr, mlx->img_ptr, 0, 0);
}
