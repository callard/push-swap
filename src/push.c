/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 13:38:14 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 17:11:53 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

/*
** pa : push a - prend le premier élément au sommet de b et le met sur a.
**	Ne fait rien si b est vide.
** pb : push b - prend le premier élément au sommet de a et le met sur b.
**	Ne fait rien si a est vide.
*/

void		ft_pa(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;

	if (b == NULL)
		return ;
	tmp = *a;
	*a = *b;
	*b = (*b)->next;
	(*a)->next = tmp;
	if (prt == 'y')
		ft_putendl("pa");
}

void		ft_pb(t_pile **a, t_pile **b, char prt)
{
	t_pile	*tmp;

	if (a == NULL)
		return ;
	tmp = *b;
	*b = *a;
	*a = (*a)->next;
	(*b)->next = tmp;
	if (prt == 'y')
		ft_putendl("pb");
}

void		ft_pa_mult(t_pile **a, t_pile **b, char prt, int mult)
{
	while (mult > 0)
	{
		ft_pa(a, b, prt);
		mult--;
	}
}
