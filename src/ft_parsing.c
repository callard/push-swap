/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 18:01:15 by callard           #+#    #+#             */
/*   Updated: 2019/02/19 14:31:31 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			ft_isdup(t_pile **a, int nb)
{
	t_pile	*tmp;

	if (a == NULL || *a == NULL)
		return (0);
	tmp = *a;
	while (tmp)
	{
		if (nb == tmp->nb)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int			ft_strisint(char *str)
{
	int		i;
	int		nb;

	i = 0;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i])
	{
		if (!(ft_isdigit(str[i])))
			return (0);
		i++;
	}
	nb = ft_atoi(str);
	if ((ft_strcmp(str, ft_itoa(nb)) != 0))
		return (0);
	return (1);
}

int			ft_strisopt(t_data *data, char *str)
{
	int		i;

	i = 0;
	if (str[i] != '-')
		return (0);
	i++;
	if ((ft_strcmp(str, "-v") == 0))
	{
		data->opt_v = 1;
		return (1);
	}
	if ((ft_strcmp(str, "-p") == 0))
	{
		data->opt_p = 1;
		return (1);
	}
	return (0);
}

t_data		*ft_validate_arg(char **av, int ac)
{
	t_data	*data;
	int		nb;

	if (!(data = (t_data *)ft_memalloc(sizeof(t_data))))
		return (NULL);
	if (!(data->a = (t_pile **)ft_memalloc(sizeof(t_pile*))))
		return (NULL);
	if (!(data->b = (t_pile **)ft_memalloc(sizeof(t_pile*))))
		return (NULL);
	while (ft_strisopt(data, av[0]))
	{
		ac--;
		av = &av[1];
	}
	while (ac >= 0)
	{
		nb = ft_atoi(av[ac]);
		if (!(ft_strisint(av[ac])) || ft_isdup(data->a, nb))
			return (NULL);
		ft_pile_add(data->a, ft_pile_new(nb));
		ac--;
	}
	ft_set_data(data);
	return (data);
}
