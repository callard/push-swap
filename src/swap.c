/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 14:45:04 by callard           #+#    #+#             */
/*   Updated: 2020/02/29 16:54:35 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

/*
** sa : swap a - intervertit les 2 premiers éléments au sommet de la pile a.
**	Ne fait rien s’il n’y en a qu’un ou aucun.
** sb : swap b - intervertit les 2 premiers éléments au sommet de la pile b.
**	Ne fait rien s’il n’y en a qu’un ou aucun.
** ss : sa et sb en même temps.
*/

void	ft_sa(t_pile **a, t_pile **b, char prt)
{
	int		tmp;

	if (a == NULL || (*a) == NULL || (*a)->next == NULL)
		return ;
	tmp = (*a)->nb;
	(*a)->nb = (*a)->next->nb;
	(*a)->next->nb = tmp;
	b = NULL;
	if (prt == 'y')
		ft_putendl("sa");
}

void	ft_sb(t_pile **a, t_pile **b, char prt)
{
	int		tmp;

	if (b == NULL || (*b) == NULL || (*b)->next == NULL)
		return ;
	tmp = (*b)->nb;
	(*b)->nb = (*b)->next->nb;
	(*b)->next->nb = tmp;
	a = NULL;
	if (prt == 'y')
		ft_putendl("sb");
}

void	ft_ss(t_pile **a, t_pile **b, char prt)
{
	ft_sa(a, b, 'n');
	ft_sb(a, b, 'n');
	if (prt == 'y')
		ft_putendl("ss");
}

void	ft_opt_sa(t_pile **a, t_pile **b, char prt)
{
	if (a != NULL && ft_pile_len(a) >= 2 && (*a)->nb > (*a)->next->nb)
	{
		ft_sa(a, NULL, prt);
	}
	b = NULL;
}
