/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:34:40 by callard           #+#    #+#             */
/*   Updated: 2018/11/14 13:59:51 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t				i;
	char				*dstbis;
	const unsigned char	*srcbis;
	unsigned char		cbis;

	i = 0;
	dstbis = dst;
	srcbis = src;
	cbis = c;
	while (i < n)
	{
		dstbis[i] = srcbis[i];
		if (srcbis[i] == cbis)
			return (&dstbis[i + 1]);
		i++;
	}
	return (NULL);
}
