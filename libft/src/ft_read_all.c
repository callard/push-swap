/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_all.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 12:29:34 by callard           #+#    #+#             */
/*   Updated: 2018/12/26 16:29:34 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

static char		*ft_read_while(int fd)
{
	char	buf[BUFF_SIZE + 1];
	char	*str;
	char	*tmp;
	int		ret;

	if (!(str = (char*)ft_strnew(BUFF_SIZE)))
		return (NULL);
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		if (ret == -1)
			return (NULL);
		buf[ret] = '\0';
		if (!(tmp = ft_strdup(str)))
			return (NULL);
		free(str);
		if (!(str = ft_strjoin(tmp, buf)))
			return (NULL);
		free(tmp);
	}
	return (str);
}

char			**ft_read_all(char *file)
{
	int		fd;
	char	*str;
	char	**tab;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		return (NULL);
	str = ft_read_while(fd);
	tab = ft_strsplit(str, '\n');
	free(str);
	if (close(fd) == -1)
		return (NULL);
	return (tab);
}
