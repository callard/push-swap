/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 12:09:15 by callard           #+#    #+#             */
/*   Updated: 2018/11/27 12:09:54 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtoupper(char *str)
{
	char	*upp_str;
	int		i;

	i = 0;
	if (!(upp_str = ft_strnew(ft_strlen(str))))
		return (NULL);
	while (str[i])
	{
		upp_str[i] = ft_toupper(str[i]);
		i++;
	}
	return (upp_str);
}
