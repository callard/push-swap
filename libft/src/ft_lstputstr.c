/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstputstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 14:31:29 by callard           #+#    #+#             */
/*   Updated: 2018/11/13 15:41:04 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstputstr(t_list *alst)
{
	while (alst)
	{
		ft_putstr((*alst).content);
		ft_putstr("\n");
		alst = (*alst).next;
	}
}
