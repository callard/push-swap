/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_table_size.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/23 14:16:05 by callard           #+#    #+#             */
/*   Updated: 2018/12/23 14:19:47 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void		ft_free_table_size(char **tab, int size)
{
	int		i;

	i = 0;
	while (i < size)
	{
		ft_strdel(&tab[i]);
		i++;
	}
	free(tab);
}
