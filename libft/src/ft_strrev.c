/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 12:07:59 by callard           #+#    #+#             */
/*   Updated: 2018/11/12 12:12:17 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *str)
{
	char	*rev;
	int		i;
	int		j;

	i = ft_strlen(str) - 1;
	j = 0;
	rev = ft_strnew(ft_strlen(str));
	while (i >= 0)
	{
		rev[j] = str[i];
		i--;
		j++;
	}
	return (rev);
}
