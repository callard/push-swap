/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:58:18 by callard           #+#    #+#             */
/*   Updated: 2018/11/14 12:06:32 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t				i;
	const unsigned char	*sbis;
	unsigned char		cbis;

	i = 0;
	sbis = s;
	cbis = c;
	while (i < n)
	{
		if (sbis[i] == cbis)
			return ((void *)&s[i]);
		i++;
	}
	return (NULL);
}
