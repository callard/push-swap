/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:28:52 by callard           #+#    #+#             */
/*   Updated: 2018/11/13 19:46:32 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t		i;
	char		*dstbis;
	const char	*srcbis;

	i = 0;
	dstbis = dst;
	srcbis = src;
	while (i < n)
	{
		dstbis[i] = srcbis[i];
		i++;
	}
	return (dst);
}
