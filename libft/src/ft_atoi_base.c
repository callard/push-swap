/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:42:58 by callard           #+#    #+#             */
/*   Updated: 2018/12/22 19:30:10 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_nb_base(char c)
{
	if (ft_isdigit(c))
		return (c - '0');
	else if (ft_isalpha(c))
		return (c - 'A' + 10);
	return (-1);
}

int				ft_atoi_base(const char *str, int base)
{
	int		len;
	int		tab[ft_strlen(str)];
	int		nb;
	int		i;

	i = 0;
	nb = 0;
	len = ft_strlen(str);
	while (i < len)
	{
		tab[i] = ft_nb_base(str[i]);
		i++;
	}
	i = 0;
	while (i < len)
	{
		nb = nb + (tab[i] * ft_pow(base, len - 1 - i));
		i++;
	}
	return (nb);
}
