/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 16:37:19 by callard           #+#    #+#             */
/*   Updated: 2018/11/14 16:50:26 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	*ft_strtrim(char const *s)
{
	int		deb;
	int		fin;
	char	*ptr;

	deb = 0;
	if (s == NULL)
		return (NULL);
	fin = ft_strlen(s) - 1;
	while (ft_isspace(s[deb]) == 1 && deb < fin)
		deb++;
	if (deb == fin)
	{
		if (!(ptr = ft_strnew(1)))
			return (NULL);
		return (ptr);
	}
	while (ft_isspace(s[fin]) == 1)
		fin--;
	if (!(ptr = ft_strnew(fin - deb + 1)))
		return (NULL);
	ft_strncpy(ptr, &s[deb], fin - deb + 1);
	ptr[fin - deb + 2] = '\0';
	return (ptr);
}
