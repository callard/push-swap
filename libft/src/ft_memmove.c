/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:39:39 by callard           #+#    #+#             */
/*   Updated: 2018/11/14 15:18:44 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*dstbis;
	const char	*srcbis;

	dstbis = dst;
	srcbis = src;
	if (dstbis < srcbis)
		ft_memcpy(dst, src, len);
	else
	{
		while (len--)
			dstbis[len] = srcbis[len];
	}
	return (dst);
}
