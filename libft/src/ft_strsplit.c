/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 12:29:51 by callard           #+#    #+#             */
/*   Updated: 2018/12/14 13:57:47 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nbwords(char const *s, char c)
{
	int		nb;
	int		i;

	nb = 0;
	i = 0;
	while (s[i])
	{
		if (s[i] != c)
		{
			nb += 1;
			while (s[i] && s[i] != c)
				i++;
		}
		else
			i++;
	}
	return (nb);
}

static int	ft_splitrec(char const *s, char **tab, char c)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (s[i] == c)
		i++;
	if (s[i] == '\0')
	{
		tab[0] = 0;
		return (1);
	}
	while (s[i + len] != c && s[i + len])
		len++;
	if (!(tab[0] = ft_strnew(len)))
		return (0);
	ft_strncpy(tab[0], &s[i], len);
	if (!(ft_splitrec(&s[i + len], &tab[1], c)))
		return (0);
	return (1);
}

char		**ft_strsplit(char const *s, char c)
{
	char **tab;

	if (s == NULL)
		return (NULL);
	if (!(tab = (char**)ft_memalloc(sizeof(char *) * (ft_nbwords(s, c) + 1))))
		return (NULL);
	if (!(ft_splitrec(s, tab, c)))
		return (NULL);
	return (tab);
}
