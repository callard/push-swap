/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <callard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 13:11:25 by callard           #+#    #+#             */
/*   Updated: 2019/02/13 11:31:08 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

char	*ft_find_fd(t_list **lst, const int fd)
{
	t_list			*tmp;
	char			*str;

	tmp = *lst;
	while (tmp)
	{
		if ((int)tmp->content_size == fd)
			return ((char*)tmp->content);
		tmp = tmp->next;
	}
	if (!(str = ft_strnew(BUFF_SIZE)))
		return (NULL);
	str = ft_memset(str, '.', BUFF_SIZE);
	if (!(tmp = ft_lstnew(str, fd)))
		return (NULL);
	free(str);
	ft_lstadd(lst, tmp);
	ft_memset((*lst)->content, '\0', BUFF_SIZE);
	return ((char*)(*lst)->content);
}

int		ft_readuntileol(const int fd, char **str)
{
	int				ret;
	char			buf[BUFF_SIZE + 1];
	char			*tmp;

	while ((ft_strchr(*str, '\n') == NULL)
			&& (ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if (!(tmp = ft_strdup(*str)))
			return (-1);
		free(*str);
		if (!(*str = ft_strjoin(tmp, buf)))
			return (-1);
		free(tmp);
	}
	if (ret == -1)
		return (-1);
	if (ret == 0 && (*str)[0] == '\0')
		return (0);
	return (1);
}

int		ft_separateresult(char **line, char *fin, char *str)
{
	int				pos;
	char			*tmp;

	pos = 0;
	tmp = ft_strchr(str, '\n');
	if (tmp == NULL)
	{
		if (!(*line = ft_strdup(str)))
			return (0);
		ft_strclr(fin);
	}
	else
	{
		pos = ft_strlen(str) - ft_strlen(tmp);
		if (!(*line = ft_strsub(str, 0, pos)))
			return (0);
		tmp++;
		ft_strclr(fin);
		fin = ft_strcpy(fin, tmp);
	}
	free(str);
	return (1);
}

int		get_next_line(const int fd, char **line)
{
	static t_list	**lst;
	int				res;
	char			*str;
	char			*tmp;

	res = 1;
	if (fd == -1)
		return (-1);
	if (lst == NULL)
		if (!(lst = (t_list **)ft_memalloc(sizeof(t_list*))))
			return (-1);
	if (!(str = ft_find_fd(lst, fd)))
		return (-1);
	if (!(tmp = ft_strdup(str)))
		return (-1);
	if (ft_strchr(tmp, '\n') == NULL)
		res = ft_readuntileol(fd, &tmp);
	if (res <= 0)
	{
		free(tmp);
		return (res);
	}
	if (!(ft_separateresult(line, str, tmp)))
		return (-1);
	return (1);
}
