/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_set_table.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: callard <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 14:18:31 by callard           #+#    #+#             */
/*   Updated: 2018/12/22 12:21:42 by callard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_create_set_table(int len, int hei, char c)
{
	char		**tab;
	int			i;

	i = 0;
	if (!(tab = (char**)ft_memalloc(sizeof(char*) * hei)))
		return (NULL);
	while (i < hei)
	{
		if (!(tab[i] = ft_strnew(len)))
			return (NULL);
		ft_memset(tab[i], c, len);
		i++;
	}
	return (tab);
}
